Tên dự án: Quản lý mã nguồn phần mềm funix Passpord
Mã sinh viên của người làm Assignment: FX22760
Repo này sử dụng cho mục đích gì? Có những branch nào và mỗi branch sẽ có những sự thay đổi gì?

Repo: Đây là một repository (repo) dành cho dự án FUNiX Passport. Dự án này có một extension liên quan đến việc hiển thị phụ đề trên các video từ nguồn Udemy.

Branches:

master: Là branch chính, chứa phiên bản ổn định của dự án. Được sử dụng cho triển khai sản phẩm.

bug/clear_console_log: Branch mới tạo để xóa các câu lệnh console.log từ mã nguồn để loại bỏ log debug không cần thiết.

feat/auto_enable_subtitle: Branch mới tạo để thêm chức năng tự động hiển thị phụ đề khi có hỗ trợ. Sửa hàm pageLoad() để thực hiện chức năng này.

document: Branch mới tạo để cập nhật tài liệu dự án. Bao gồm lưu các sơ đồ và file .docx vào thư mục documentation, sau đó chuyển đổi file .docx thành định dạng Markdown và lưu vào documentation/Document.md.

backend: Branch mới tạo để cập nhật thông tin và source code liên quan đến phần Backend của dự án. Bao gồm file backend.md trong thư mục backend với các thông tin về vai trò và chức năng của Backend.